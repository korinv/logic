4<?php

class Site extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('about_model');
    }

    public function  view() {
        $data['rec'] = $this->about_model->get_records();
        $this->load->view('first',$data);
    }

    // function  load1() {
    //     //$data['myvalue'] = "some string";
    //     //$data['anothervalue'] = "some another string";
    //     $this->load->model('about_model');
    //    $data['records'] = $this->about_model->getall();
    //     $this->load->view('first', $data);
    // }
    // function  display1() {
    //    $this->load->model('about_model');
    //     $data['rec'] = $this->about_model->get_records();
    //     $this->load->view('first', $data);
    // }
    function store(){
        $data = $this->about_model->add_records();
        $this->view();
    }
}
?>


